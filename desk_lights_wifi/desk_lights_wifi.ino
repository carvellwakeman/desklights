/******************************************************************************
* Desk Lighting Wifi - Zach
* 
*******************************************************************************/

#include "secrets.h"
#include "html.h"

//#include <ArduinoJson.h>
#include "FastLED.h"
#include "Arduino.h"
//#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ArduinoOTA.h>


/* LED Definitions */
#define NUM_LEDS 108
#define CHIPSET NEOPIXEL
#define DATA_PIN 5

/* Clients */
ESP8266WebServer server(80);
WiFiClient espClient;


/* LED Variables */
struct CRGB leds[NUM_LEDS];
int idx = 0;
byte red = 0;
byte green = 0;
byte blue = 0;
byte hue = 0;
String pattern = "";


/* Animation */
int anim_step = 0;
int anim_step_max = 255; //Inclusive [0, 255]
int anim_step_ms = 10000;
unsigned long anim_next_step_ms = 0;
unsigned long anim_curr_step_ms = 0;
unsigned long anim_prev_step_ms = 0;
bool anim_change = false;


/* Gradients */


/* Arduino Setup */
void setup()
{
    Serial.begin(115200);
    Serial.print("Starting...");
    
    // LEDs
    FastLED.addLeds<CHIPSET, DATA_PIN>(leds, NUM_LEDS);
    FillStrip(CRGB::Black);
    FlushStrip();

    // Connect to WiFi network
    WiFi.begin(WIFI_SSID, WIFI_PASS);

    while (WiFi.status() != WL_CONNECTED) {
        delay(80);
    }

    // OTA SETUP
    ArduinoOTA.setHostname(OTA_NAME);
    ArduinoOTA.setPassword((const char *)OTA_PASSWORD);
    ArduinoOTA.setPort(OTA_PORT);

    ArduinoOTA.onStart([]() {
        PatternOTAUpdate(true);
        Serial.println("Starting");
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        idx = map(progress, 0, total, 0, NUM_LEDS);
        PatternOTAUpdate(false);
        Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    });
    ArduinoOTA.onEnd([]() {
        FillStrip(CRGB::ForestGreen);
        FlushStrip();
        Serial.println("\nEnd");
    });
    ArduinoOTA.onError([](ota_error_t error) {
        FillStrip(CRGB::Red);
        FlushStrip();
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
        else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
        else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
        else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
        else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });
    ArduinoOTA.begin();

    // Clear strip for patterns
    FillStrip(CRGB::Black);
    FlushStrip();

    // Start the server
    server.begin();

    // Register routes
    server.on("/strip", StripRoute);
    server.on("/set", PatternRoute);
    server.on("/get", GetPatternRoute);
    server.on("/", GetWebsiteRoute);

    // Print the IP address
    Serial.print(WiFi.localIP());
}


/* Arduino Loop */
void loop()
{
    ProcessAnimation();
    ProcessPattern(false);

    ArduinoOTA.handle();

    server.handleClient();

    delay(10);
}


// Routes
void StripRoute()
{
    String data = GetArgumentValue("plain");
    SetFromStripData(data);
    
    if (data == "")
        server.send(400, "text/plain", "Bad parameter:" + data);

    server.send(200, "text/plain", "LED");
}

void PatternRoute()
{
    String patternInput = GetArgumentValue("pattern");
    byte brightness = GetArgumentValue("brightness").toInt();
    int anim_speed = GetArgumentValue("anim_speed").toInt();
    
    if (patternInput == ""){
        server.send(400, "text/plain", "Bad parameter:" + GetArgumentValue("pattern"));
        return;
    }

    // Global parameters
    if (brightness > 0)
        FastLED.setBrightness(brightness);
    if (anim_speed > 0)
        anim_step_ms = anim_speed;

    PatternBegin(patternInput);
    
    server.send(200, "text/plain", patternInput);
}

void GetPatternRoute()
{
    server.send(200, "text/plain", pattern);
}

void GetWebsiteRoute()
{
    server.send(200, "text/html", MAIN_page);
}


/* ANIMATION */
void ProcessAnimation()
{
    // Breakout
    if (pattern == "") { return; }

    anim_change = false;
    
    anim_next_step_ms = millis();
    anim_curr_step_ms = anim_next_step_ms - anim_prev_step_ms;

    if (anim_curr_step_ms >= anim_step_ms) {
        anim_prev_step_ms = anim_next_step_ms;
        anim_change = true;
    }

    anim_step = map(anim_curr_step_ms, 0, anim_step_ms, 0, anim_step_max);
}


/* PATTERNS */
void ProcessPattern(bool init)
{
    // Breakout
    if (pattern == "") { return; }

    // Solid
    if (pattern == "led")
    {
        PatternIndividualLED(init);
    }
    else if (pattern == "solid")
    {
        PatternSolidColor(init);
    }
    else if (pattern == "hsv_strip")
    {
        PatternHSVStrip(init);
    }

    // Animated
    else if (pattern == "hsv_cycle")
    {
        PatternHSVCycle(init);
    }
    else if (pattern == "wipe")
    {
        PatternLeftToRight(init);
    }
    else if (pattern == "wave")
    {
        PatternSinWave(init);
    }
    else if (pattern == "confetti")
    {
        PatternConfetti(init);
    }
    else if (pattern == "rain")
    {
        PatternRain(init);
    }
    else if (pattern == "hsv_rain")
    {
        PatternHSVRain(init);
    }
    else if (pattern == "fire")
    {
        PatternFire(init);
    }
    else if (pattern == "lightning")
    {
        PatternLightning(init);
    }
    else if (pattern == "test")
    {
        PatternTest(init);
    }
}

// Pattern start and stop
void PatternBegin(String newPattern)
{
    // Reset
    FastLED.setBrightness(255);
    idx = 0;
    red = 0;
    green = 0;
    blue = 0;
    hue = 0;
    anim_prev_step_ms = 0;
    anim_curr_step_ms = 0;
    anim_next_step_ms = 0;
    anim_step = 0;
    FillStrip(CRGB::Black);
    FlushStrip();
    // Pattern init
    pattern = newPattern;
    ProcessPattern(true);
}
void PatternEnd() { pattern = ""; }

// Solid
void PatternIndividualLED(bool init)
{
    if (init)
    {
        GetIndexValue();
        GetRGBValues();
        SetPixelColor(idx, red, green, blue);
        FlushStrip();
        PatternEnd();
    }
}
void PatternSolidColor(bool init)
{
    if (init)
    {
        GetRGBValues();
        FillStrip(red, green, blue);
        FlushStrip();
        PatternEnd();
    }
}
void PatternHSVStrip(bool init)
{
    if (init)
    {
        for(int i=0; i < NUM_LEDS; i++)
        {
            SetPixelColorHSV(i, map(i, 0, NUM_LEDS, 0, 255), 255, 255);
        }
        FlushStrip();
    }
}

// Animated
void PatternOTAUpdate(bool init)
{
    if (init)
    {
        FillStrip(CRGB::DarkCyan);
    } else {
        SetPixelColor(idx, CRGB::DarkOrange);
        FlushStrip();
    }
}
void PatternWifiConnect(bool init)
{
    if (init)
    {
        // Dimmer forest green
        FillStrip(14, 119, 14);
    }
    else
    {
        uint16_t wave = beatsin8(20);
        for (int i=0; i < NUM_LEDS; i++)
        {
            leds[i].r = map(wave, 0, 255, 0, 14);
            leds[i].g = map(wave, 0, 255, 0, 119);
            leds[i].b = map(wave, 0, 255, 0, 14);
        }
        FlushStrip();
    }
}
void PatternHSVCycle(bool init)
{
    FillStripHSV(MapAnim(0, 255), 255, 255);
    FlushStrip();
}
void PatternConfetti(bool init)
{
    if (init){
        GetRGBValues();
    }

    fadeToBlackBy(leds, NUM_LEDS, 25);
    int pos = random16(NUM_LEDS);
    leds[pos] += CRGB(red + random8(64), green, blue);

    FlushStrip();
}
void PatternLeftToRight(bool init)
{
    if (init)
    {
        anim_step_max = NUM_LEDS-1;
    } else {
        FillStrip(CRGB::Black);
        SetPixelColor(anim_step, 0,0,255);
        FlushStrip();
    }
}
void PatternSinWave(bool init)
{
    if (init)
    {
        GetRGBValues();
    }
    else {
        uint8_t outer = beatsin8(30, 0, NUM_LEDS - 1);
        leds[outer] = CRGB::Aqua;
        nscale8(leds, NUM_LEDS, red);
        FlushStrip();
    }
}
byte fade;
byte freq;
void PatternRain(bool init)
{
    if (init)
    {
        GetRGBValues();
        fade = GetInt(GetArgumentValue("fade"), 254);
        freq = GetInt(GetArgumentValue("freq"), 128);
    }
    else {
        if (random8(0, 255) > freq)
        {
            uint8_t led = random8(0, NUM_LEDS - 1);
            SetPixelColor(led, red, green, blue);
        }
        if (anim_change)
        {
            nscale8(leds, NUM_LEDS, fade);
        }
        FlushStrip();
    }
}
void PatternHSVRain(bool init)
{
    if (init)
    {
        fade = GetInt(GetArgumentValue("fade"), 254);
        freq = GetInt(GetArgumentValue("freq"), 120);
    }
    else {
        if (random8(0, 255) > freq)
        {
            uint8_t led = random8(0, NUM_LEDS - 1);
            SetPixelColor(led, CHSV(hue++, 255, 255));
        }
        nscale8(leds, NUM_LEDS, fade);
        FlushStrip();
    }
}

#define COOLING  55
#define SPARKING 120
bool gReverseDirection = false;
CRGBPalette16 gPal = HeatColors_p; //for fire
void PatternFire(bool init)
{
    // Array of temperature readings at each simulation cell
    static byte heat[NUM_LEDS];

    // Step 1.  Cool down every cell a little
    for ( int i = 0; i < NUM_LEDS; i++) {
        heat[i] = qsub8( heat[i],  random8(0, ((COOLING * 10) / NUM_LEDS) + 2));
    }

    // Step 2.  Heat from each cell drifts around and diffuses a little
    for ( int k = NUM_LEDS - 2; k >= 1; k--) {
        heat[k] = (heat[k - 1] + 2*heat[k - 2] ) / 3;
    }

    // Step 3.  Randomly ignite new 'sparks' of heat randomly
    if ( random8() < SPARKING ) {
        int y = random8(NUM_LEDS);
        heat[y] = qadd8( heat[y], random8(160, 255) );
    }

    // Step 4.  Map from heat cells to LED colors
    for ( int j = 0; j < NUM_LEDS; j++) {
        // Scale the heat value from 0-255 down to 0-240
        // for best results with color palettes.
        byte colorindex = scale8( heat[j], 240);
        CRGB color = ColorFromPalette( gPal, colorindex);
        int pixelnumber;
        if ( gReverseDirection ) {
            pixelnumber = (NUM_LEDS - 1) - j;
        } else {
            pixelnumber = j;
        }
        SetPixelColor(pixelnumber, color);
    }

    FlushStrip();
}

uint8_t frequency = 50;                                       // controls the interval between strikes
uint8_t flashes = 8;                                          //the upper limit of flashes per strike
unsigned int dimmer = 1;
uint8_t ledstart;                                             // Starting location of a flash
uint8_t ledlen;
int lightningcounter = 0;
void PatternLightning(bool init)
{
    ledstart = random8(NUM_LEDS);           // Determine starting location of flash
    ledlen = random8(NUM_LEDS - ledstart);  // Determine length of flash (not to go beyond NUM_LEDS-1)
    for (int flashCounter = 0; flashCounter < random8(3, flashes); flashCounter++) {
        if (flashCounter == 0) dimmer = 5;    // the brightness of the leader is scaled down by a factor of 5
        else dimmer = random8(1, 3);          // return strokes are brighter than the leader
        fill_solid(leds + ledstart, ledlen, CHSV(255, 0, 255 / dimmer));
        FlushStrip();
        delay(random8(4, 10));                // each flash only lasts 4-10 milliseconds
        fill_solid(leds + ledstart, ledlen, CHSV(255, 0, 0)); // Clear the section of LED's
        FlushStrip();
        if (flashCounter == 0) delay (130);   // longer delay until next flash after the leader
        delay(50 + random8(100));             // shorter delay between strokes
    }
    delay(random8(frequency) * 100);        // delay between strikes

    FlushStrip();
}


void PatternTest(bool init)
{
    // Array of temperature readings at each simulation cell
    static byte wave[NUM_LEDS];

    // Step 1.  Cool down every cell a little
    // for ( int i = 0; i < NUM_LEDS; i++) {
    //     wave[i] = qsub8( wave[i],  random8(0, ((COOLING * 10) / NUM_LEDS) + 2));
    // }

    // // Step 2. Each cell tries to reach an even height
    // static byte waveDiffLeft;
    // static byte waveDiffRight;
    // for ( int k = 1; k < NUM_LEDS-1; k++) {
    //     waveDiffLeft = (wave[k] - wave[k-1]) / 2;
    //     waveDiffRight = (wave[k] - wave[k+1]) / 2;
    //     wave[k] = 
    //     wave[k+1] = (wave[k] - wave[k+1]) / 2;
    //     wave[k] = (wave[k] + wave[k-1]) / 2;
    //     //heat[k] = (heat[k - 1] + 2*heat[k - 2] ) / 3;
    // }

    // // Step 3.  Randomly ignite new 'sparks' of heat randomly
    // if ( random8() < SPARKING ) {
    //     int y = random8(NUM_LEDS);
    //     wave[y] = qadd8( heat[y], random8(160, 255) );
    // }

    // // Step 4.  Map from heat cells to LED colors
    // for ( int j = 0; j < NUM_LEDS; j++) {
    //     // Scale the heat value from 0-255 down to 0-240
    //     // for best results with color palettes.
    //     byte colorindex = scale8( heat[j], 240);
    //     CRGB color = ColorFromPalette( gPal, colorindex);
    //     int pixelnumber;
    //     if ( gReverseDirection ) {
    //         pixelnumber = (NUM_LEDS - 1) - j;
    //     } else {
    //         pixelnumber = j;
    //     }
    //     SetPixelColor(pixelnumber, color);
    // }

    // FlushStrip();

    // // First slide the led in one direction
    // for (int i = 0; i < NUM_LEDS; i++) {
    //     SetPixelColorHSV(i, CHSV(anim_step+i, 255, 255));
    //     FlushStrip();

    //     // Reset the i'th led to black
    //     //leds[i] = CRGB::Black;
    //     // for (int i = 0; i < NUM_LEDS; i++) {
    //     //     leds[i].nscale8(250);
    //     // }
    //     //delay(10);
    // }

    // for (int i = (NUM_LEDS) - 1; i >= 0; i--) {
    //     SetPixelColorHSV(i, CHSV(hue++, 255, 255));
    //     FlushStrip();
        
    //     // Reset the i'th led to black
    //     //leds[i] = CRGB::Black;
    //     for (int i = 0; i < NUM_LEDS; i++) {
    //         leds[i].nscale8(250);
    //     }
    //     delay(10);
    // }


    // CRGBPalette16 palette = heatmap_gp;
    // for(int i=0; i < NUM_LEDS; i++)
    // {
    //     int index = map(i, 0, NUM_LEDS-1, 0, 255);
    //     SetPixelColor(i, ColorFromPalette(palette, index));
    // }
    

    // uint8_t BeatsPerMinute = 62;
    // uint8_t beat = beatsin8(BeatsPerMinute, 64, 255);
    // for(int i=0; i < NUM_LEDS; i++)
    // {
    //     leds[i] = ColorFromPalette(palette, hue + (i * 2), beat - hue + (i * 10));
    // }

    //FlushStrip();
}


/* ROUTE HELPERS */
int GetInt(String value, int default_value)
{
    if (value == "")
    {
        return default_value;
    }
    return value.toInt();
}

String GetArgumentValue(String name)
{
    if (server.hasArg(name))
    {
        return server.arg(name);
    }
    return "";
}

void GetIndexValue()
{
    idx = GetInt(GetArgumentValue("i"), -1);

    //if (idx < 0)
    //    server.send(400, "text/plain", "Bad parameter:" + GetArgumentValue("i"));
    
}

void GetRGBValues()
{
    red = GetArgumentValue("r").toInt();
    green = GetArgumentValue("g").toInt();
    blue = GetArgumentValue("b").toInt();
    
    //if (red < 0 || green < 0 || blue < 0)
    //    server.send(400, "text/plain", "Bad parameter:" + GetArgumentValue("r") + "," + GetArgumentValue("g") + "," + GetArgumentValue("b"));
}


/* LED HELPERS */
void SetPixelColor(int i, byte r, byte g, byte b)
{
    leds[i].setRGB(r, g, b);
}
void SetPixelColor(int i, CRGB color)
{
    leds[i] = color;
}

void SetPixelColorHSV(int i, byte hue, byte sat, byte val)
{
    leds[i].setHSV(hue, sat, val);
}

void SetPixelColorHSV(int i, CHSV color)
{
    leds[i] = color;
}

void FillStrip(CRGB color)
{
    fill_solid(leds, NUM_LEDS, color);
}

void FillStrip(byte r, byte g, byte b)
{
    fill_solid(leds, NUM_LEDS, CRGB(r,g,b));
}

void FillStripHSV(byte hue, byte sat, byte val)
{
    fill_solid(leds, NUM_LEDS, CHSV(hue, sat, val));
}

void FadeStrip(int percent)
{
    for(int i=0; i < NUM_LEDS; i++)
    {
        leds[i].fadeToBlackBy(map(percent, 0, 100, 0, 256));
    }
    FlushStrip();
}

void FlushStrip()
{
    FastLED.show();
}

void SetFromStripData(String stripData)
{
    int start=0;
    String ledData = "";

    for (int current=0; current < stripData.length(); current++)
    {
        // Pull number before separator, or last number
        if(stripData.charAt(current) == ',')
        {
            ledData = stripData.substring(start, current);
            SetFromLEDData(ledData);
            start=current+1; 
        }

        if (current == stripData.length() - 1)
        {
            ledData = stripData.substring(start, current+1);
            SetFromLEDData(ledData);
        }
    }

    FastLED.show();
}

void SetFromLEDData(String ledData)
{
    //idx:r:g:b
    int sep1 = ledData.indexOf(':');
    int sep2 = ledData.indexOf(':', sep1+1);
    int sep3 = ledData.indexOf(':', sep2+1);
    idx = ledData.substring(0, sep1).toInt();
    red = ledData.substring(sep1+1, sep2-1).toInt();
    green = ledData.substring(sep2+1, sep3-1).toInt();
    blue = ledData.substring(sep3+1, ledData.length()-1).toInt();

    SetPixelColor(idx, red, green, blue);
}


/* MATH HELPERS */
int MapAnim(int min, int max)
{
    return map(anim_step, 0, anim_step_max, min, max);
}
