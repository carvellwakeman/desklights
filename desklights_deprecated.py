#################################
# Desk lighting - Zach
# Installation:
# pip install pyserial flask
# Run with Python3 (py -3)
#################################


# Imports
import time
import struct
import random
import math
import threading
import requests
from flask import Flask, request

# Global
current_thread = None

# Constants
NUM_LEDS = 108
SEND_DELAY_SC_MS = 10
SEND_DELAY_MS = 100
REQUEST_TIMEOUT = 2
BRIGHTNESS = 100
ARDUINO_IP = "http://192.168.1.13"
PORT = 9142
LEDS = []


class PatternThread(threading.Thread):
    def __init__(self, pattern, kwargs, name='PatternThread'):
        self._pattern = pattern
        self._patternargs = kwargs
        self._stopevent = threading.Event()
        #self._sleepperiod = SEND_DELAY_SC_MS/1000
        threading.Thread.__init__(self, name=name)
    
    def run(self):
        last = None
        while not self._stopevent.isSet( ):
            last = self._pattern(**self._patternargs, package=last)
            if last == None: break
            #self._stopevent.wait(self._sleepperiod)

    def join(self, timeout=None):
        self._stopevent.set( )
        threading.Thread.join(self, timeout)


# Endpoint
app = Flask(__name__)


def main():
    # Init LEDS
    initStrip()

    # Start Flask
    app.run(debug=False, host='0.0.0.0', port=PORT)

    # Run pattern
    #SolidColor((255,0,255))
    #SolidColorPartial(range(5), (255,255,255))
    #HsvCycle()
    #HsvStrip(2)
    #Wipe(20, hsv=True)


# Flask Routes
@app.route('/off')
def flask_off():
    run_pattern(pattern=SolidColor, kwargs=dict(color=(0,0,0)))
    return ""
@app.route('/brightness')
def flask_brightness():
    global BRIGHTNESS
    b = int(request.args.get('b') or 100)
    BRIGHTNESS = min(max(b,0), 100)
    return ""
@app.route('/solid') #<int:r>
def flask_solid():
    r = int(request.args.get('r') or 0)
    g = int(request.args.get('g') or 0)
    b = int(request.args.get('b') or 0)
    
    run_pattern(pattern=SolidColor, kwargs=dict(color=(r,g,b)))
    return ""
@app.route('/hsv/cycle')
def flask_hsvCycle():
    t = int(request.args.get('t') or 100)
    run_pattern(pattern=HsvCycle, kwargs=dict(speed=t))
    return ""
@app.route('/hsv/strip')
def flask_hsvStrip():
    t = int(request.args.get('t') or 0)
    run_pattern(pattern=HsvStrip, kwargs=dict(diff=t))
    return ""
@app.route('/colorwipe')
def flask_wipe():
    r = int(request.args.get('r') or 0)
    g = int(request.args.get('g') or 0)
    b = int(request.args.get('b') or 0)
    hsv = not r and not g and not b
    run_pattern(pattern=Wipe, kwargs=dict(BAR_LENGTH=10, BAR_COLOR=(r,g,b), hsv=hsv))
    return ""
@app.route('/flash')
def flask_flash():
    r = int(request.args.get('r') or 0)
    g = int(request.args.get('g') or 0)
    b = int(request.args.get('b') or 0)
    t = int(request.args.get('t') or 1)
    run_pattern(pattern=Flash, kwargs=dict(color=(r,g,b), speed=t))
    return ""

def run_pattern(pattern, kwargs={}):
    global current_thread

    if current_thread is not None:
        current_thread.join()
    
    current_thread = PatternThread(pattern=pattern, kwargs=kwargs)
    current_thread.start()


# Patterns
def SolidColorPartial(leds, color, package=0):
    for led in leds:
        flushSingleLED(color, led)

    return None

def SolidColor(color, package=0):
    # Write colors to internal state
    flushSingle(color)

    return None


def HsvCycle(package=0, speed=100):
    STEP_SIZE = 1000

    mStep = package or 0
    steps = range(STEP_SIZE)
    if mStep >= len(steps):
        return 0
        
    color = getColor(hsvToRgb(float(steps[mStep]) / float(STEP_SIZE), 1.0, 1.0), BRIGHTNESS)
    SolidColor(color, mStep)
    
    time.sleep(speed/STEP_SIZE)

    return mStep+1


def HsvStrip(package=0, diff=0):
    for l in range(NUM_LEDS):
        p = l + (package or 0)
        while p > NUM_LEDS-1:
            p -= NUM_LEDS

        color = getColor(hsvToRgb(float(l) / float(NUM_LEDS), 1.0, 1.0), BRIGHTNESS)
        LEDS[p] = color

    flushStrip()
    
    return (package or 0)+diff


def Wipe(BAR_LENGTH, BAR_COLOR = [255,0,0], OFF_COLOR = [0,0,0], hsv=False, package=[0,0]):
    p = package or [0, 1]
    head = p[0]
    headDir = p[1]
        
    # Start the wipe
    if hsv:
        BAR_COLOR = hsvToRgb(float(head)/float(NUM_LEDS), 1, 1)
    
    # Color the bar
    LEDS[head] = getColor(BAR_COLOR, BRIGHTNESS)
    
    # Set bounds of bar to off color
    if head >= 0:
        LEDS[head - (BAR_LENGTH*headDir)] = OFF_COLOR
    if head < NUM_LEDS:
        LEDS[head + (headDir)] = OFF_COLOR

    # Advance head and check bounds
    head += headDir
    if head >= NUM_LEDS-1:
        headDir *= -1
        head -= BAR_LENGTH
    if head <= 0:
        headDir *= -1
        head += BAR_LENGTH
    flushStrip()

    return [head, headDir]
        

def Flash(package=0, color=(255,255,255), speed=100):
    STEP_SIZE = 100

    mStep = package or 0
    steps = range(STEP_SIZE)
    if mStep >= len(steps):
        return 0
    
    pct = float(steps[mStep]) / float(STEP_SIZE)
    color = getColor(interpolate(color, (0,0,0), pct), BRIGHTNESS)
    SolidColor(color, mStep)
    
    time.sleep(speed/STEP_SIZE)

    return mStep+1


# Send one color for one LED
def flushSingleLED(color, ledIdx):
    try:
        r = requests.get(ARDUINO_IP + '/led?i=' + str(ledIdx) + '&r=' + str(color[0]) + '&g=' + str(color[1]) + '&b=' + str(color[2]), timeout=REQUEST_TIMEOUT)
        #print(r.url)

        time.sleep(SEND_DELAY_SC_MS/1000)
    except:
        print("Request Exception in single LED")
        pass

# Send one color for whole strip
def flushSingle(color):
    try:
        r = requests.get(ARDUINO_IP + '/fill?r=' + str(color[0]) + '&g=' + str(color[1]) + '&b=' + str(color[2]), timeout=REQUEST_TIMEOUT)
        #print(r.url)

        time.sleep(SEND_DELAY_SC_MS/10000)
    except:
        print("Request Exception in fill")
        pass

# Send entire strip state
def flushStrip():
    try:
        body = stripString()
        r = requests.post(ARDUINO_IP + '/strip', json=body, timeout=REQUEST_TIMEOUT)
        #print(r.url)
        time.sleep(SEND_DELAY_SC_MS/10000)
    except:
        print("Request Exception in strip")
        pass


# LED strip
def initStrip():
    for i in range(NUM_LEDS):
        LEDS.append((0,0,0))
def clearStrip():
    for i in range(NUM_LEDS):
        LEDS[i] = (0,0,0)
def stripString():
    strip = [str(i)+":"+str(rgb[0])+":"+str(rgb[1])+":"+str(rgb[2]) for i, rgb in enumerate(LEDS)]
    return ",".join(strip)


# Modify color by brightness
def getColor(color, brightness):
    br = float(brightness) / 100.0
    r = min(color[0],255) or 0
    g = min(color[1],255) or 0
    b = min(color[2],255) or 0
    return [round(r * br), round(g * br), round(b * br)]

# Percentage of color
def interpolate(color1, color2, factor):
    result = [color1[0], color1[1], color1[2]]
    for i in range(3):
        result[i-1] = round(result[i-1] + factor * (color2[i-1] - color1[i-1]))
    
    return result


# HSV to RGB
def hsvToRgb(hue, sat, val):
    # Input validation
    h = max(0.0, min(hue*6.0, 6.0))
    s = max(0.0, min(sat, 1.0))
    v = max(0.0, min(val, 1.0))

    # Gray
    if s == 0.0:
        return [round(v*255), round(v*255), round(v*255)]

    # Get rgb from the hue itself
    i = int(math.floor(h))
    f = h - i
    p = v * (1.0 - s)
    q = v * (1.0 - s * f)
    t = v * (1.0 - (s * (1.0 - f)))

    # Final output
    r = 0
    g = 0
    b = 0
    if i == 0:
        r = v
        g = t
        b = p
    elif i == 1:
        r = q
        g = v
        b = p
    elif i == 2:
        r = p
        g = v
        b = t
    elif i == 3:
        r = p
        g = q
        b = v
    elif i == 4:
        r = t
        g = p
        b = v
    elif i == 5:
        r = v
        g = p
        b = q

    return [round(r*255), round(g*255), round(b*255)]


# Windows
if __name__ == '__main__':
    main()
