/******************************************************************************
* Desk Lighting - Zach
* Make sure your USB type is Serial, running at speed 9600
*******************************************************************************/
/*#include <Wire.h>*/
#include "Arduino.h"
#include "FastLED.h"

#define NUM_LEDS 108       // adjust to your length of LED strip*/
#define LED_DATA_PIN 2    // adjust to the used pin (Arduino nano pin 2 = D2)

#define rxPin 0 /* Reciever pin */
#define txPin 1 /* Transmitter pin */

#define BAUD_RATE 115200 /* Baud rate for serial */

/* Serial Receive buffer size (+1 for mode flag)*/ 
#define ARDUINO_RX_BUFF_SIZE 57

/* Temp variables */
CRGB leds[NUM_LEDS];
int bufferSize, serialLen;
unsigned char readBuffer[ARDUINO_RX_BUFF_SIZE], ledBuffer[ARDUINO_RX_BUFF_SIZE];
unsigned char m, i, r, g, b;

void setup() {
  FastLED.addLeds<NEOPIXEL, LED_DATA_PIN>(leds, NUM_LEDS);  // initialize LEDs

  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  Serial.begin(BAUD_RATE); /* serial port */

  // Reset LEDs to black
  memset(leds, 0, NUM_LEDS);
  FastLED.show();

  bufferSize = 0;
}


void loop()
{
    while (Serial.available() > 0) {
        ledBuffer[bufferSize] = Serial.read();
        bufferSize += 1;
    }

    // Mode check
    if (bufferSize >= 4 && ledBuffer[0] == 1) { // Single color
        r = ledBuffer[1];
        g = ledBuffer[2];
        b = ledBuffer[3];
        
        fill_solid(leds, NUM_LEDS, CRGB(r,g,b));
        FastLED.show();

        bufferSize = 0;
    } else if (bufferSize >= 5 && ledBuffer[0] == 2) { // Single LED single color
        r = ledBuffer[1];
        g = ledBuffer[2];
        b = ledBuffer[3];
        i = ledBuffer[4];

        leds[i].r = r;
        leds[i].g = g;
        leds[i].b = b;
        FastLED.show();

        bufferSize = 0;
    } else if (bufferSize >= ARDUINO_RX_BUFF_SIZE) { // Color per index
        for(int l = 1; l < ARDUINO_RX_BUFF_SIZE; l+=4) {
            i = ledBuffer[l + 0];
            r = ledBuffer[l + 1];
            g = ledBuffer[l + 2];
            b = ledBuffer[l + 3];

            leds[i].r = r;
            leds[i].g = g;
            leds[i].b = b;
            FastLED.show();
        }

        bufferSize = 0;
    }
}

