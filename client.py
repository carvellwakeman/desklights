#################################
# Desk lighting client
# Run with Python3 (py -3)
#################################


# Imports
import requests
import time
from ctypes import windll

# Constants
ADDR = "http://192.168.1.70"
PORT = 9142
SAMPLES = 100


def main():
    hdc = windll.user32.GetDC(0)
    lastColor = 0
    width = 1920
    height = 1080

    while True:
        # Get screen color
        color = getScreenColor(hdc, width, height)

        if color != lastColor:
            lastColor = color

            # Convert to RGB
            r,g,b = getRGBfromInt(color)
            
            # Send request
            requests.get(ADDR + ":" + str(PORT) + '/solid?r=' + str(r) + '&g=' + str(g) + '&b=' + str(b))


# Helpers
# https://stackoverflow.com/questions/33124347/convert-integers-to-rgb-values-and-back-with-python
def getRGBfromInt(RGBint):
    red =  RGBint & 255
    green = (RGBint >> 8) & 255
    blue =   (RGBint >> 16) & 255
    return red, green, blue

def getScreenColor(hdc, width, height):
    global SAMPLES

    color = n = 0
    for x in range(0, width, int(width/SAMPLES)):
        for y in range(0, height, int(height/SAMPLES)):
            n += 1
            color += windll.gdi32.GetPixel(hdc, x, y)

    return int(color / n)


# Windows
if __name__ == '__main__':
    main()

